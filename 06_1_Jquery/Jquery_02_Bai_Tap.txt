BTVN
	1. So sánh 2 số, xuất ra số lớn hơn.
		Cho một cấu trúc HTML như bên dưới, khi chạy file thì so sánh 2 số a và b bên dưới, in số lớn hơn vào: <div id="result"></div>

	
		<!doctype html>
		<html lang="en">
		<head>
		<meta charset="utf-8">
		<title>Học web chuẩn</title>
		<script src="https://code.jquery.com/jquery-latest.js"></script>
		<script>
		$(function(){
			var a = 5;
			var b = 6;
			
			// viết thêm code ở đây
		});
		</script>
		</head>

		<body>
		<div id="result"></div>
		</body>
		</html>
	2. Sử dụng Toggle
		Cho một cấu trúc HTML như bên dưới, khi click button thì thành phần div sẽ luân phiên ẩn hiện.
		Sử dụng gợi ý code ở link: https://www.w3schools.com/jquery/tryit.asp?filename=tryjquery_toggle

			!doctype html>
			<html lang="en">
			<head>
			<meta charset="utf-8">
			<title>Học web chuẩn</title>
			<script src="https://code.jquery.com/jquery-latest.js"></script>
			<style>
			  .box {
				border: 3px solid #eee;
				display: none;
				height: 100px;
				margin-top: 5px;
				padding: 10px;
				width: 150px;
			  }
			</style>
			<script>
			  $(function(){
				/* Code viết ở đây ↓ */

			  });
			</script>
			</head>

			<body>
			<button type="button" id="toggle">Open</button>
			<div class="box">In suscipit nec velit sit amet convallis. Integer</div>
			</body>
			</html>

- Thời gian nộp
		Trước 8 tiếng của buổi học tiếp theo