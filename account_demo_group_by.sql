
CREATE TABLE if NOT EXISTS account_demo_group_by (
  id INT PRIMARY KEY AUTO_INCREMENT,
  first_name VARCHAR(255),
  last_name VARCHAR(255),
  salary INT,
  department_name VARCHAR(255)
);

INSERT INTO account_demo_group_by (id, first_name, last_name, salary, department_name)
VALUES
	(1, 'Nguyễn', 'Ngọc', 1000, 'Sale'),
	(2, 'Trần', 'Hải', 1200, 'Marketing'),
	(3, 'Phạm', 'Ngọc', 1300, 'Marketing'),
	(4, 'Phạm', 'Nam', 1500, 'IT'),
	(5, 'Nguyễn', 'Hà', 1800, 'Sale'),
	(6, 'Trần', 'Dương', 900, 'IT');
