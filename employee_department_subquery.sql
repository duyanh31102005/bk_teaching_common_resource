CREATE TABLE employee_subquery (
	id INT PRIMARY KEY AUTO_INCREMENT, 
	first_name VARCHAR(255),
	last_name VARCHAR(255),
	department_name  VARCHAR(255)
);

CREATE TABLE department_subquery (
	id INT PRIMARY KEY AUTO_INCREMENT,
	department_code VARCHAR(255)
	department_name  VARCHAR(255)
);

INSERT INTO employee_subquery (id, first_name, last_name, department_name)
VALUES
	(1, 'Nguyễn', 'Ngọc', 'Nhân Sự'),
	(2, 'Trần', 'Nam', 'Công Nghệ Thông Tin'),
	(3, 'Nguyễn', 'Hải', 'Nhân Sự'),
	(4, 'Nguyễn', 'Ngọc', 'Kế Toán');

INSERT INTO department_subquery (id, department_code, department_name)
VALUES
	(1, 'NS', 'Ngọc', 'Nhân Sự'),
	(2, 'CNTT_PM', 'Nam', 'Công Nghệ Thông Tin Phần Mềm'),
	(3, 'CNTT_PC', 'Nam', 'Công Nghệ Thông Tin Phần Cứng'),
	(4, 'KT', 'Kế Toán'),
	(5, 'CNTP_CB', 'Công Nghệ Thực Phẩm Chế Biến'),
	(6, 'CNTP_NC', 'Công Nghệ Thực Phẩm Nghiên Cứu'),
	(7, 'Quản Lý Dự Án', 'QLDA');