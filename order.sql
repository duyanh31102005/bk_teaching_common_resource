CREATE TABLE order (
	id INT,
	product_name VARCHAR(50),
	price INT,
	quantity INT,
	supplier VARCHAR(50),
	tax INT,
	imported_date DATETIME,
	from_country VARCHAR(50),
	created_date DATETIME,
	updated_date DATETIME
);

INSERT INTO order (id, product_name, price, quantity, supplier, tax, import_date, from_country)
VALUES
	(1, 'Wine', 41, 6, 'Sunhouse', 10, '2022-04-01', 'France'),
	(2, 'Flower', 40, 7, 'Amazone', 12, '2022-04-01', 'France'),
	(3, 'Flower', 39, 7, 'Amazone', 13, '2022-04-01', 'Germany'),
	(4, 'Rice', NULL, 4, 'Oracle', NULL, NULL, 'France'),
	(5, 'Flower', 37, 7, 'Sunhouse', 14, '2022-04-01', 'France'),
	(6, 'Sugar', 38, 8, 'Sunhouse', 11, '2022-04-01', 'France'),
	(7, 'Sugar', NULL, 4, 'Amazone', NULL, NULL, 'Germany'),
	(8, 'Rice', 38, 4, 'Oracle', 9, '2022-04-01', 'Germany'),
	(9, 'Wine', 41, 6, 'Oracle', 12, '2022-04-01', 'France'),
	(10, 'Bread', 40, 7, 'Oracle', 14, '2022-04-01', 'France'),
	(11, 'Rice', 39, 8, 'Sun Shades Lip Balm', 14, '2022-04-01', 'France'),
	(12, 'Sugar', 41, 4, 'Oracle', 13, '2022-04-01', 'France'),
	(13, 'Bread', NULL, 7, 'Oracle', NULL, NULL, 'France'),
	(14, 'Sugar', 38, 8, 'Sunhouse', 6, '2022-04-01', 'France'),
	(15, 'Wine', NULL, 5, 'Sunhouse', NULL, NULL, 'France'),
	(16, 'Wine', 37, 7, 'Amazone', 7, '2022-04-01', 'France'),
	(17, 'Rice', 40, 4, 'Sunhouse', 14, '2022-04-01', 'France'),
	(18, 'Bread', 38, 7, 'Amazone', 10, '2022-04-01', 'France'),
	(19, 'Rice', 40, 6, 'Oracle', 8, '2022-04-01', 'France'),
	(20, 'Sugar', 39, 5, 'Amazone', 14, '2022-04-01', 'France');
