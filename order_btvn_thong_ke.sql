Btvn
(Dùng bảng order ở https://gitlab.com/t0013/teaching_common_resource/-/blob/master/order.sql) để làm những yêu cầu sau
1. Có bao nhiêu sản phẩm được nhập vào năm 2022
2. Tính giá trung bình các sản phẩm do Oracle cung cấp
3. Hiển thị các sản phẩm không biết giá trị thuế
4. Hiển thị tên các nhà cung cấp mà có ít nhất 1 sản phẩm chưa có giá trị thuế