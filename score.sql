-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.28 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for fullstack1
CREATE DATABASE IF NOT EXISTS `fullstack1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `fullstack1`;

-- Dumping structure for table fullstack1.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) DEFAULT NULL,
  `price` int DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `active` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `tax` int unsigned DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table fullstack1.product: ~29 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `product_name`, `price`, `amount`, `active`, `tax`, `created_date`, `updated_date`) VALUES
	(1, 'Rượu', 16, 4, 'false', 46, '2021-12-18', '2022-09-12'),
	(2, '	Muối', 36, 5, 'false', 13, '2021-11-13', '2022-03-05'),
	(3, 'Trứng', 32, 16, 'false', 5, '2022-08-05', '2022-04-22'),
	(4, 'Thịt Bò', 44, 13, 'true', 56, '2022-08-23', '2022-04-09'),
	(5, 'Bánh Mì', 41, 17, 'false', 64, '2019-07-18', '2022-12-23'),
	(6, 'Hạt Tiêu', 24, 13, '	false', 86, '2022-02-19', '2022-08-25'),
	(7, 'Bia', 17, 18, 'false', 6, '2022-03-14', '2022-10-15'),
	(8, 'Khoai Tây', 31, 9, '	true', 39, '2021-12-04', '2022-01-03'),
	(9, 'Hoa', 25, 9, 'false', 65, '2019-05-21', '2022-01-17'),
	(10, 'Bắp cải', 48, 7, 'true', 27, '2021-11-04', '2022-03-13'),
	(11, 'Cua', 24, 14, 'false', 48, '2022-01-09', '2022-02-11'),
	(12, 'Đào khô', 37, 17, 'true', 89, '2022-07-31', '2022-12-06'),
	(13, 'Mù tạt', 46, 2, 'true', 28, '2022-04-15', '2022-10-08'),
	(14, 'Kiwi', 30, 14, 'false', 81, '2022-06-21', '2022-04-24'),
	(15, 'Rượu', 16, 4, 'false', 46, '2021-12-18', '2022-09-12'),
	(16, '	Muối', 36, 5, 'false', 13, '2021-11-13', '2022-03-05'),
	(17, 'Trứng', 32, 16, 'false', 5, '2022-08-05', '2022-04-22'),
	(18, 'Thịt Bò', 44, 13, 'true', 56, '2022-08-23', '2022-04-09'),
	(19, 'Bánh Mì', 41, 17, 'false', 64, '2019-07-18', '2022-12-23'),
	(20, 'Hạt Tiêu', 24, 13, '	false', 86, '2022-02-19', '2022-08-25'),
	(21, 'Bia', 17, 18, 'false', 6, '2022-03-14', '2022-10-15'),
	(22, 'Khoai Tây', 31, 9, '	true', 39, '2021-12-04', '2022-01-03'),
	(23, 'Hoa', 25, 9, 'false', 65, '2019-05-21', '2022-01-17'),
	(24, 'Bắp cải', 48, 7, 'true', 27, '2021-11-04', '2022-03-13'),
	(25, 'Cua', 24, 14, 'false', 48, '2022-01-09', '2022-02-11'),
	(26, 'Đào khô', 37, 17, 'true', 89, '2022-07-31', '2022-12-06'),
	(27, 'Mù tạt', 46, 2, 'true', 28, '2022-04-15', '2022-10-08'),
	(28, 'Kiwi', 30, 14, 'false', 81, '2022-06-21', '2022-04-24'),
	(29, 'Dầu ăn', 19, NULL, NULL, NULL, '2023-03-28', NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
